use thiserror::Error;
use xand_api_client::{errors::XandApiClientError, AddressError, TransactionId, TransactionStatus};

#[derive(Debug, Error)]
pub enum Error {
    #[error("Error during address conversion: {:?}", 0)]
    AddressConversion(#[from] AddressError),
    #[error("{:?}", 0)]
    InferError(String),
    #[error("{:?}", 0)]
    EntityNotFound(String),
    #[error("{:?}", 0)]
    XandNetworkGeneratorError(String),
    #[error("{:?}", 0)]
    XandApiClientError(#[from] XandApiClientError),
    #[error("Transaction {:?} failed: {:?}", 0, 1)]
    TxnFailed(TransactionId, TransactionStatus),
    #[error("{:?}", 0)]
    GeneralTestAssertionFailure(String),
    #[error("There were some acceptance tests that did not pass.")]
    AcceptanceTestsDidNotPass,
}
