use crate::{
    puppeteer::Puppeteer,
    testing_ports::{
        network_entities::{actions::EntityActions, queries::EntityQueries, EntityAddress},
        scenario::{safe_assert_eq, Scenario, ScenarioResult},
    },
    Error,
};
use async_trait::async_trait;
use xand_api_client::{AddAuthorityKey, Address, AdministrativeTransaction, XandApiClientTrait};

/// Arbitrary address to propose
const NEW_VAL_ADDRESS: &str = "5CcEng4N9XZsngPrQ74kksRipnR8yFaZ4VC23UsUHJM7PYoZ";
const SCENARIO_NAME: &str = "limited-agent-proposes-and-cannot-vote";

/// In this scenario, the LimitedAgent proposes a new validator. We assert the
/// proposal has no votes on it (ensuring the LA does not have any vote counted).
/// And then we try actively voting on it with the LA, asserting that the attempt fails.
/// The proposal is then left open, with no more entities voting on it.
pub struct LimitedAgentProposesAndCannotVote {
    new_val_address: Address,
}

impl LimitedAgentProposesAndCannotVote {
    pub fn default() -> Self {
        Self {
            new_val_address: NEW_VAL_ADDRESS.parse().unwrap(),
        }
    }
}
#[async_trait]
impl Scenario for LimitedAgentProposesAndCannotVote {
    fn get_name(&self) -> String {
        SCENARIO_NAME.into()
    }

    async fn run_scenario(&self) -> ScenarioResult {
        let puppeteer = Puppeteer::from_default_path()?;
        let limited_agent = puppeteer.get_limited_agent();
        let entity_address = EntityAddress::XandAddress(limited_agent.0.address.parse().unwrap());
        info!(
            "LimitedAgent proposing AddAuthorityKey: {}",
            &self.new_val_address
        );
        let add_validator_proposal = AdministrativeTransaction::AddAuthorityKey(AddAuthorityKey {
            account_id: self.new_val_address.clone(),
        });
        let prop_id = puppeteer
            .propose(entity_address.clone(), add_validator_proposal)
            .await?;

        info!("Asserting no votes for proposal_id: {}", &prop_id);
        let xand_api_client = puppeteer.connect_entity(entity_address.clone()).await?;
        let prop = xand_api_client.get_proposal(prop_id).await?;
        safe_assert_eq(
            &prop.votes.is_empty(),
            &true,
            Error::GeneralTestAssertionFailure(format!(
                "Proposal just submitted by LimitedAgent has unexpected votes: {:?}",
                prop.votes
            )),
        )?;

        info!(
            "LimitedAgent attempting to vote on proposal_id: {}",
            &prop_id
        );
        let vote_result = puppeteer
            .vote_for_proposal(entity_address.clone(), prop_id, true)
            .await;

        dbg!(&vote_result);

        let prop = xand_api_client.get_proposal(prop_id).await?;
        safe_assert_eq(
            &prop.votes.is_empty(),
            &true,
            Error::GeneralTestAssertionFailure(format!(
                "After attempted vote, proposal just submitted by LimitedAgent has unexpected votes: {:?}",
                prop.votes
            )),
        )?;

        Ok(())
    }
}
