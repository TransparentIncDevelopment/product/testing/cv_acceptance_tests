use std::str::FromStr;

use async_trait::async_trait;
use xand_api_client::{Address, AdministrativeTransaction, SetTrustNodeId, XandApiClientTrait};

use crate::puppeteer::Puppeteer;
use crate::testing_ports::network_entities::actions::EntityActions;
use crate::testing_ports::network_entities::queries::EntityQueries;
use crate::testing_ports::network_entities::EntityAddress;
use crate::testing_ports::scenario::{safe_assert_eq, Scenario};
use crate::{Error, Result};

// Index of member submitting proposal and querying chain for membership
const PROPOSING_MEMBER_INDEX: usize = 0;

// Nonexistent participant used as a stand-in for a new trust node. AzDev #5824
const NEW_TRUST_ADDRESS: &str = "5CcEng4N9XZsngPrQ74kksRipnR8yFaZ4VC23UsUHJM7PYoZ";

const SCENARIO_NAME: &str = "vote-in-new-trust";

pub struct AssignNewTrustScenario {
    proposer_member: EntityAddress,
    new_trust_address: Address,
}

impl AssignNewTrustScenario {
    pub fn default() -> Self {
        Self::new(
            PROPOSING_MEMBER_INDEX,
            Address::from_str(NEW_TRUST_ADDRESS).expect("static constant"),
        )
    }

    pub fn new(proposer_member_index: usize, new_trust_address: Address) -> Self {
        AssignNewTrustScenario {
            proposer_member: EntityAddress::MemberIndex(proposer_member_index),
            new_trust_address,
        }
    }

    async fn validate_preconditions(&self, puppeteer: &Puppeteer) -> Result<()> {
        info!(
            "Using member {:?} to read entities registered on-chain",
            self.proposer_member
        );

        let proposer_client = puppeteer
            .connect_entity(self.proposer_member.clone())
            .await?;
        let on_chain_members = proposer_client.get_members().await?;
        debug!("On-chain members: {:?}", on_chain_members);

        info!(
            "Asserting member {:?} (proposer) is on-chain",
            self.proposer_member
        );

        let proposer_address =
            puppeteer.get_xand_address_of_entity(self.proposer_member.clone())?;

        safe_assert_eq(
            &on_chain_members.contains(&proposer_address),
            &true,
            Error::GeneralTestAssertionFailure(format!(
                "This test requires proposer ({:?}) to be on-chain, but was not found",
                proposer_address
            )),
        )?;

        info!(
            "Asserting address {} is not assigned as the trust",
            self.new_trust_address
        );
        let perceived_trustee_address = proposer_client.get_trustee().await?;
        safe_assert_eq(
            &perceived_trustee_address.eq(&self.new_trust_address),
            &false,
            Error::GeneralTestAssertionFailure(format!(
                "Trustee is currently {:?}, who must begin unassigned for this test",
                perceived_trustee_address
            )),
        )?;

        Ok(())
    }
}

#[async_trait]
impl Scenario for AssignNewTrustScenario {
    fn get_name(&self) -> String {
        SCENARIO_NAME.to_string()
    }

    async fn run_scenario(&self) -> Result<()> {
        let puppeteer = Puppeteer::from_default_path()?;

        self.validate_preconditions(&puppeteer).await?;

        info!(
            "Proposing assigning new trust: {:?}",
            self.new_trust_address
        );
        let set_trust_proposal = AdministrativeTransaction::SetTrust(SetTrustNodeId {
            address: self.new_trust_address.clone(),
            encryption_key: [0xF0u8; 32].into(),
        });
        let proposal_id = puppeteer
            .propose(self.proposer_member.clone(), set_trust_proposal)
            .await?;
        puppeteer.vote_unanimously(proposal_id, true).await?;

        info!("Asserting that onboarding member sees new trust...");
        let proposer_client = puppeteer
            .connect_entity(self.proposer_member.clone())
            .await?;
        let perceived_trustee_address = proposer_client.get_trustee().await?;
        safe_assert_eq(
            &perceived_trustee_address.eq(&self.new_trust_address),
            &true,
            Error::GeneralTestAssertionFailure(format!(
                "Expected {} to be assigned as trustee, but trustee is actually {}",
                self.new_trust_address, perceived_trustee_address
            )),
        )?;
        info!("Confirmed trust is now {}", &self.new_trust_address);
        Ok(())
    }
}
