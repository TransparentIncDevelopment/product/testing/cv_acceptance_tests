use async_trait::async_trait;
use xand_api_client::{AdministrativeTransaction, RegisterAccountAsMember, XandApiClientTrait};

use crate::puppeteer::Puppeteer;
use crate::testing_ports::network_entities::actions::EntityActions;
use crate::testing_ports::network_entities::queries::EntityQueries;
use crate::testing_ports::network_entities::EntityAddress;
use crate::testing_ports::scenario::{safe_assert_eq, Scenario, ScenarioResult};
use crate::utilities::address_extension::AddressExt;
use crate::Error;

// Index of member submitting proposal and querying chain for membership
const PROPOSING_MEMBER_INDEX: usize = 0;

// Index of new member to add to chain
const NEW_MEMBER_INDEX: usize = 8;

const SCENARIO_NAME: &str = "vote-in-member";

pub struct VoteInMemberScenario {
    proposer_member_index: usize,
    new_member_index: usize,
}

impl VoteInMemberScenario {
    pub fn default() -> Self {
        Self::new(PROPOSING_MEMBER_INDEX, NEW_MEMBER_INDEX)
    }

    pub fn new(proposer_member_index: usize, new_member_index: usize) -> Self {
        VoteInMemberScenario {
            proposer_member_index,
            new_member_index,
        }
    }
}

#[async_trait]
impl Scenario for VoteInMemberScenario {
    fn get_name(&self) -> String {
        SCENARIO_NAME.to_string()
    }

    async fn run_scenario(&self) -> ScenarioResult {
        // Load all entities from config
        let puppeteer = Puppeteer::from_default_path()?;
        let onboarder_puppet_address = EntityAddress::MemberIndex(self.proposer_member_index);

        info!(
            "Using member {} to read entities registered on-chain",
            self.proposer_member_index
        );
        let on_chain_members = puppeteer
            .connect_entity(onboarder_puppet_address.clone())
            .await?
            .get_members()
            .await?;
        debug!("On-chain members: {:?}", &on_chain_members);

        info!(
            "Asserting member {} is not found on-chain",
            self.new_member_index
        );
        let new_member_addr = puppeteer
            .get_member_by_index(self.new_member_index)?
            .as_address();
        safe_assert_eq(
            &on_chain_members.contains(&new_member_addr),
            &false,
            Error::GeneralTestAssertionFailure(format!(
                "Found {:?} ({}) on-chain when expected to be unlisted",
                self.new_member_index, new_member_addr,
            )),
        )?;
        let add_member_proposal =
            AdministrativeTransaction::RegisterAccountAsMember(RegisterAccountAsMember {
                address: new_member_addr.clone(),
                encryption_key: [0xFFu8; 32].into(),
            });
        let proposal_id = puppeteer
            .propose(onboarder_puppet_address.clone(), add_member_proposal)
            .await?;
        puppeteer.vote_unanimously(proposal_id, true).await?;

        info!("Assert that onboarding member can find new member in chain state.");
        let on_chain_members = puppeteer
            .connect_entity(onboarder_puppet_address)
            .await?
            .get_members()
            .await?;
        safe_assert_eq(
            &on_chain_members.contains(&new_member_addr),
            &true,
            Error::GeneralTestAssertionFailure(format!(
                "Did not find {:?} ({}) on-chain when expected to be listed",
                self.new_member_index, new_member_addr,
            )),
        )?;

        info!("Confirmed membership for {:?}", &new_member_addr);
        Ok(())
    }
}
