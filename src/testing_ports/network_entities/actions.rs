use async_trait::async_trait;
use xand_api_client::{AdministrativeTransaction, XandApiClient};

use crate::testing_ports::network_entities::EntityAddress;
use crate::Result;

/// Trait for actions that can be taken with entities on a generated network
#[async_trait]
pub trait EntityActions {
    /// Directly connects a new XandApiClient for the entity and returns it
    async fn connect_entity(&self, entity: EntityAddress) -> Result<XandApiClient>;
    /// Uses the entity to make a given proposal, then awaits its finalization and returns the id of the proposal made
    async fn propose(
        &self,
        entity: EntityAddress,
        proposal: AdministrativeTransaction,
    ) -> Result<u32>;
    /// Uses all available voters to cast a vote for the given proposal in the given direction.
    /// Waits for all votes to be finalized on-chain.
    async fn vote_unanimously(&self, proposal_id: u32, vote_direction: bool) -> Result<()>;
    /// Uses the entity to vote for a given proposal in a given direction and awaits finalization of that vote on the chain
    async fn vote_for_proposal(
        &self,
        entity: EntityAddress,
        proposal_id: u32,
        vote_direction: bool,
    ) -> Result<()>;
}
