use std::future::Future;
use std::time::Duration;

/// Utility struct to retry a closure for a given amount of time.
/// This is useful when tests need to query and assert chain state after some activity,
/// where the change in chain state might take some time to be reflected across different Xandstrate Nodes
pub struct Retrier {
    delay: Duration,
    attempts: usize,
}

impl Default for Retrier {
    /// Returns a Retrier that will try every 100ms for 30s
    fn default() -> Self {
        let delay = Duration::from_millis(100);
        let attempts: usize = 300; // 30,000 ms / 100 attempts = 200

        Self::new(delay, attempts)
    }
}

impl Retrier {
    fn new(delay: Duration, attempts: usize) -> Self {
        Self { delay, attempts }
    }

    /// Retry the closure until it returns `Ok`
    pub async fn retry_until_ok<Fn, Fut, T, E>(self, closure: Fn) -> Result<T, E>
    where
        Fut: Future<Output = Result<T, E>> + std::marker::Send,
        Fn: FnMut() -> Fut,
        E: std::fmt::Debug,
    {
        let policy = again::RetryPolicy::fixed(self.delay).with_max_retries(self.attempts);
        policy.retry(closure).await
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::atomic::{AtomicUsize, Ordering};

    #[test]
    fn default__delay_is_100_ms_over_30_seconds() {
        let x = Duration::from_secs(30) / 1000;
        dbg!(x);
        // When
        let retrier = Retrier::default();

        // Then
        assert_eq!(retrier.delay, Duration::from_millis(100));
        let total_duration = retrier.delay * retrier.attempts as u32;
        assert_eq!(total_duration, Duration::from_secs(30));
    }

    #[tokio::test]
    async fn retry_until_ok__given_fn_returning_ok_returns_ok() {
        // Given
        let delay = Duration::from_millis(10);
        let attempts = 10;
        let retrier = Retrier::new(delay, attempts);
        async fn returns_ok() -> Result<(), ()> {
            Ok(())
        }

        // When
        let res = retrier.retry_until_ok(returns_ok).await;

        // Then
        assert_eq!(res, Ok(()))
    }

    #[tokio::test]
    async fn retry_until_ok__given_fn_returning_err_returns_err() {
        // Given
        let delay = Duration::from_millis(10);
        let attempts = 10;
        let retrier = Retrier::new(delay, attempts);
        async fn returns_err() -> Result<(), ()> {
            Err(())
        }

        // When
        let res = retrier.retry_until_ok(returns_err).await;

        // Then
        assert_eq!(res, Err(()))
    }

    #[tokio::test]
    async fn retry_until_ok__given_fn_returning_ok_on_2nd_call_returns_ok() {
        // Given
        let delay = Duration::from_millis(10);
        let attempts = 10;
        let retrier = Retrier::new(delay, attempts);
        let test_struct = SomeStatefulCallable::default();

        // When
        let res = retrier.retry_until_ok(|| test_struct.ok_on_2nd()).await;

        // Then
        assert_eq!(res, Ok(42))
    }

    #[tokio::test]
    async fn ok_on_2nd__returns_err_on_first_call() {
        // Given
        let x = SomeStatefulCallable::default();

        // When
        let res = x.ok_on_2nd().await;

        // Then
        assert!(res.is_err());
    }

    #[tokio::test]
    #[allow(unused_must_use)]
    async fn ok_on_2nd__returns_ok_on_2nd_call() {
        // Given
        let x = SomeStatefulCallable::default();
        x.ok_on_2nd().await.unwrap_err(); // Call once

        // When
        let res = x.ok_on_2nd().await;

        // Then
        assert!(res.is_ok());
    }

    struct SomeStatefulCallable {
        count: AtomicUsize,
    }

    impl SomeStatefulCallable {
        pub fn default() -> Self {
            Self {
                count: AtomicUsize::new(0),
            }
        }

        async fn ok_on_2nd(&self) -> Result<usize, ()> {
            let next = self.count.fetch_add(1, Ordering::SeqCst);
            match next {
                0 => Err(()),
                1 => Ok(42),
                _ => Err(()),
            }
        }
    }
}
