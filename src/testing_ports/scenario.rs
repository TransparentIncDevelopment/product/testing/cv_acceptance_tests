use crate::{Error, Result};
use async_trait::async_trait;

pub mod retrier;

pub type ScenarioResult = Result<()>;
type NamedScenarioResult = (String, ScenarioResult);

#[async_trait]
pub trait Scenario {
    fn get_name(&self) -> String; // Note - this uses &self so that the trait is Sized
    async fn run_scenario(&self) -> ScenarioResult;
}

#[derive(Default)]
pub struct ScenarioTestRunner {
    scenarios: Vec<Box<dyn Scenario>>,
}

impl ScenarioTestRunner {
    pub fn new(scenarios: Vec<Box<dyn Scenario>>) -> Self {
        Self { scenarios }
    }

    pub async fn run_all_scenarios(&self) -> Result<()> {
        let mut results = Vec::<NamedScenarioResult>::new();

        for scenario in &self.scenarios {
            let scenario_name = scenario.get_name();
            info!("Running scenario {}", scenario_name);
            let result = scenario.run_scenario().await;
            if result.is_err() {
                error!("Scenario {} failed: {:?}", scenario_name, result);
            } else {
                info!("Scenario {} has passed", scenario_name);
            }
            results.push((scenario_name, result));
        }
        ScenarioTestRunner::assess_results(results)
    }

    fn assess_results(run_results: Vec<NamedScenarioResult>) -> Result<()> {
        info!("All tests have finished! Assessing results...");
        info!("Final test results from {} tests run:", run_results.len());
        let mut failure_count = 0;
        for (scenario_name, scenario_run_result) in run_results {
            let success = scenario_run_result.is_ok();
            let error_string = if let Err(e) = scenario_run_result {
                failure_count += 1;
                format!("\nFailure: {:?}", e)
            } else {
                "".to_string()
            };
            info!(
                "\nScenario: {}\nPass: {}{}",
                scenario_name, success, error_string
            );
        }

        if failure_count > 0 {
            error!("{} Test(s) failed to pass", failure_count);
            Err(Error::AcceptanceTestsDidNotPass)
        } else {
            Ok(())
        }
    }
}

pub fn safe_assert_eq<T: std::cmp::PartialEq>(
    a: &T,
    b: &T,
    failure_output: Error,
) -> ScenarioResult {
    if a.eq(b) {
        Ok(())
    } else {
        Err(failure_output)
    }
}
