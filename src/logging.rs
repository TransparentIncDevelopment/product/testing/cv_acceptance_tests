use env_logger::Env;

const NAME: &str = env!("CARGO_PKG_NAME");

pub fn init() {
    let config = format!("{}={}", NAME, "debug");
    env_logger::from_env(Env::default().default_filter_or(config)).init();
}
