use crate::testing_ports::network_entities::LimitedAgentEntity;
use crate::{
    testing_ports::network_entities::{
        actions::EntityActions, queries::EntityQueries, EntityAddress, MemberEntity,
        ValidatorEntity,
    },
    utilities::{
        address_extension::AddressExt,
        xand_api_helpers::{infer_id, wait_until_finalized},
    },
    Error, Result,
};
use async_trait::async_trait;
use futures::{future::join_all, TryFutureExt};
use std::path::PathBuf;
use xand_api_client::{
    Address, AdministrativeTransaction, TransactionStatusStream, VoteProposal, XandApiClient,
    XandApiClientTrait,
};
use xand_network_generator::contracts::network::metadata::xand_entity_set_summary::{
    XandApiDetails, XandEntitySetSummary,
};

pub const PUPPETEER_ENTITIES_METADATA_FILE: &str = "./generated/entities-metadata.yaml";

/// A structure which keeps track of entities on a generated network and can run actions for those entities
#[derive(Clone, Debug)]
pub struct Puppeteer {
    members: Vec<MemberEntity>,
    validators: Vec<ValidatorEntity>,
    limited_agent: LimitedAgentEntity,
}

impl Puppeteer {
    pub fn new(entity_set: XandEntitySetSummary) -> Self {
        let members: Vec<MemberEntity> = entity_set.members.into_iter().map(MemberEntity).collect();
        let validators: Vec<ValidatorEntity> = entity_set
            .validators
            .into_iter()
            .map(ValidatorEntity)
            .collect();
        let limited_agent = LimitedAgentEntity(entity_set.limited_agent);
        Puppeteer {
            members,
            validators,
            limited_agent,
        }
    }

    pub fn from_path(path: PathBuf) -> Result<Self> {
        let entities = XandEntitySetSummary::load(path).map_err(|e| {
            Error::XandNetworkGeneratorError(format!("Failed to load entities: {:?}", e))
        })?;
        Ok(Puppeteer::new(entities))
    }

    pub fn from_default_path() -> Result<Self> {
        let path: PathBuf = PUPPETEER_ENTITIES_METADATA_FILE.into();
        Puppeteer::from_path(path)
    }

    fn get_xand_api_details<A: AddressExt>(&self, addr: &A) -> Result<&XandApiDetails> {
        let addr_str = addr.as_address().to_string();
        if addr_str == self.limited_agent.0.address {
            return Ok(&self.limited_agent.0.xand_api_details);
        }
        if let Some(mem) = self.members.iter().find(|m| addr_str == m.0.address) {
            return Ok(&mem.0.xand_api_details);
        }
        if let Some(val) = self.validators.iter().find(|v| addr_str == v.0.address) {
            return Ok(&val.0.xand_api_details);
        }
        Err(Error::EntityNotFound(format!(
            "Could not find participant {}'s Xand API details.",
            addr_str
        )))
    }

    async fn submit_vote_for_entity(
        &self,
        entity: EntityAddress,
        proposal_id: u32,
        vote_direction: bool,
    ) -> Result<TransactionStatusStream> {
        let addr = self.get_xand_address_of_entity(entity)?;
        let vote_txn = VoteProposal {
            id: proposal_id,
            vote: vote_direction,
        };
        debug!(
            "{:?} submitting {:?} vote for {:?}",
            &addr.as_address(),
            &vote_direction,
            &proposal_id
        );
        let stream = self
            .connect_entity(EntityAddress::XandAddress(addr.clone()))
            .await?
            .vote_on_proposal(addr.as_address(), vote_txn)
            .await?;
        Ok(stream)
    }
}

impl EntityQueries for Puppeteer {
    fn get_limited_agent(&self) -> &LimitedAgentEntity {
        &self.limited_agent
    }

    fn get_member_by_index(&self, idx: usize) -> Result<&MemberEntity> {
        self.members
            .get(idx)
            .ok_or_else(|| Error::EntityNotFound(format!("Member @ Index {}", idx)))
    }

    fn get_validator_by_index(&self, idx: usize) -> Result<&ValidatorEntity> {
        self.validators
            .get(idx)
            .ok_or_else(|| Error::EntityNotFound(format!("Validator @ Index {}", idx)))
    }

    fn get_xand_address_of_entity(&self, entity: EntityAddress) -> Result<Address> {
        match entity {
            EntityAddress::XandAddress(addr) => Ok(addr),
            EntityAddress::MemberIndex(idx) => Ok(self.get_member_by_index(idx)?.as_address()),
            EntityAddress::ValidatorIndex(idx) => {
                Ok(self.get_validator_by_index(idx)?.as_address())
            }
        }
    }
}

#[async_trait]
impl EntityActions for Puppeteer {
    async fn connect_entity(&self, entity: EntityAddress) -> Result<XandApiClient> {
        let address = self.get_xand_address_of_entity(entity)?.as_address();
        let details = self.get_xand_api_details(&address)?;
        let url_str = details.xand_api_url.to_string();
        let url = url_str.parse().unwrap();
        let client = XandApiClient::connect(&url).await?;
        Ok(client)
    }

    async fn propose(
        &self,
        entity: EntityAddress,
        proposal: AdministrativeTransaction,
    ) -> Result<u32> {
        let proposer_address = self
            .get_xand_address_of_entity(entity.clone())?
            .as_address();
        let api_client = self.connect_entity(entity.clone()).await?;
        info!("Proposing {:?} by {:?}", proposal, entity);
        api_client
            .propose_action(proposer_address.clone().as_address(), proposal.clone())
            .map_err(Into::into)
            .and_then(wait_until_finalized)
            .await?;
        let proposal_id = infer_id(&api_client, &proposal).await?;
        info!("Proposal ID inferred: {}", proposal_id.clone());
        Ok(proposal_id)
    }

    async fn vote_unanimously(&self, proposal_id: u32, vote_direction: bool) -> Result<()> {
        info!(
            "Voting {} on proposal {} for all entities",
            vote_direction, proposal_id
        );
        let member_addresses: Vec<Address> = self.members.iter().map(|m| m.as_address()).collect();
        let validator_addresses: Vec<Address> =
            self.validators.iter().map(|v| v.as_address()).collect();
        let entities = member_addresses.iter().chain(validator_addresses.iter());
        let all_votes = entities.map(|addr| {
            self.submit_vote_for_entity(
                EntityAddress::XandAddress(addr.clone()),
                proposal_id,
                vote_direction,
            )
            .and_then(wait_until_finalized)
        });
        join_all(all_votes)
            .await
            .into_iter()
            .collect::<Result<Vec<()>>>()?;
        info!("All votes finalized");
        Ok(())
    }

    async fn vote_for_proposal(
        &self,
        entity: EntityAddress,
        proposal_id: u32,
        vote_direction: bool,
    ) -> Result<()> {
        let stream = self
            .submit_vote_for_entity(entity, proposal_id, vote_direction)
            .await?;
        wait_until_finalized(stream).await?;
        info!("Vote finalized");
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use insta::assert_debug_snapshot;

    #[test]
    fn debug_snapshot_of_puppeteer_from_default_path() {
        assert_debug_snapshot!(Puppeteer::from_default_path())
    }
}
