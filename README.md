# Council Voting Acceptance Tests

This crate solely houses Acceptance Tests related to Governance and Council Voting
that can be run against a running network.

## Dependencies

Tests use a configuration file describing all entities in a network (URLs, JWT tokens, private/public keys per entity).

Using the configuration file, tests will "puppeteer" different entities to assert network-wide behavior.

## Local Development

> When running this network here in this repo, make sure you don't have other docker-compose XAND networks
> running on your machine. You can run `docker ps` to see if there is anything else running.

These tests can be run against a local docker-compose network. See the [`xand_network_generator` repo](https://gitlab.com/TransparentIncDevelopment/product/testing/xand_network_generator/)
for steps on how to generate a local network for testing.

A testing network is currently statically checked in here in the `./generated` directory.

It contains 5 validators and 5 members configured into the chain spec.

It contains an _additional_ 5 members and validators who are not included in the chain spec. These
entities (and their corresponding APIs) can be used to test voting in and voting out entities on a running XAND network.

### Generating the test network

Install `xng` with:

```bash
make install-xng
```
> NOTE: This will globally install a specific version of XNG onto your machine.

Generate a network with:

```bash
make install-xng
make download-chain-spec
make generate
``` 

The crate binary itself runs 1 `scenario::<scenario_name>` at a time,
logging output.  Collectively, the test scenarios assume they are
starting with a fresh network and chain.

To cleanup stale state, start the network, and run the scenarios:
```bash
make run
```

### Snapshot Tests

This repo uses the `insta` crate for snapshot testing. 

Install it with:
```bash
cargo install cargo-insta
```

When you see a failing snapshot test, run 
```bash
cargo insta review
```

`cargo insta review` can help you both assess whether the new snapshot looks correct (e.g. by 
diffing with a pre-existing one) and approve it as the new authoritative snapshot

See https://insta.rs for a good intro to snapshot tests. In particular, see 
their ["Test Workflow"](https://insta.rs/#test-workflow).
